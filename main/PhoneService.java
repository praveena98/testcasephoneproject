package main;

import java.io.FileWriter;
import java.time.LocalDate;

/**
 * A interface specifies the operations on Student object
 * 
 * @author praveena prasannakumar
 *
 * 
 */
public interface PhoneService {
	/**
	 * The method will add a phone
	 * 
	 * @param phone to be added
	 * 
	 */
	void add(Phone phone);

	/**
	 * The method should display all the phones
	 */

	void displayElements();

	/**
	 * The method will search phones and display the phone with given name
	 * 
	 * @param name name of the phone to be searched
	 * 
	 */

	void search(String name);

	/**
	 * The method sorts the phones based on name
	 */

	void sort();

	/**
	 * The method will search phones and display the manufactured date with
	 * manufactured date is after the given manufactured date
	 * 
	 * @param manufactured date
	 * 
	 */

	void search(LocalDate date);

	/**
	 * This default method getCSVString append the data and return the data as csv
	 * format
	 */
	void downloadDetailsAsCSV() throws Exception;
	
	default String getCSVString(Phone phone) { // comma for csv values

		return new StringBuffer(phone.getName()).append(",").append(phone.getYear()).append(",")
				.append(phone.getPrice()).append(",").append(phone.getRam()).append(",").append(phone.getColor())
				.append(",").append(phone.getManufaturedDate()).append(",").toString();

	}

	/**
	 * This default method getJsonString append the data and return the data as json
	 * format
	 * @throws Exception 
	 */
	void downloadDetailsAsJSON() throws Exception;
	
	default String getJSONString(Phone phone) { // json
		return new StringBuffer().append("{").append("\"Phone Name\": \"").append(phone.getName()).append("\",")
				.append("\"Year\":").append(phone.getYear()).append(",").append("\"Phone Price\":")
				.append(phone.getPrice()).append(",").append("\"Ram\":").append(phone.getRam()).append(",")
				.append("\"Phone color\": \"").append(phone.getColor()).append("\",").append("\"Manufature date\":")
				.append(phone.getManufaturedDate()).append("}").toString();

	}
	
}
