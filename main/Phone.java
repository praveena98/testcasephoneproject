package main;

import java.time.LocalDate;

//base class Phone 
public class Phone implements Comparable<Phone> {
	private String name;
	private int year;
	private double price;
	private int ram;
	// enum
	private Color phoneColor;
	private LocalDate manufaturedDate;

	// Parameterized constructor
	public Phone(String name, int year, double price, int ram, Color phoneColor, LocalDate manufaturedDate) {
		this.name = name;
		this.year = year;
		this.price = price;
		this.ram = ram;
		this.phoneColor = phoneColor;
		this.manufaturedDate = manufaturedDate;
	}

	// getters and setters
	public Color getColor() {
		return phoneColor;
	}

	public void setColor(Color phoneColor) {
		this.phoneColor = phoneColor;
	}

	public LocalDate getManufaturedDate() {
		return manufaturedDate;
	}

	public void setManufaturedDate(LocalDate manufaturedDate) {
		this.manufaturedDate = manufaturedDate;
	}

	public LocalDate getmanufaturedDate() {
		return manufaturedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public void setmanufaturedDate(LocalDate manufaturedDate) {
		this.manufaturedDate = manufaturedDate;
	}

	// toString method and StringBuffer method
	public String toString() {
		return new StringBuffer().append("Phone Name:").append(name).append("\n").append("Year:").append(year)
				.append("\n").append("Price:").append(price).append("\n").append("RAM:").append(ram).append("\n")
				.append("Phone Color:").append(phoneColor).append("\n").append("Manufatured Date:")
				.append(manufaturedDate).toString();
	}

//equals method by eclipse

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		if (manufaturedDate == null) {
			if (other.manufaturedDate != null)
				return false;
		} else if (!manufaturedDate.equals(other.manufaturedDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phoneColor != other.phoneColor)
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		if (ram != other.ram)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public int compareTo(Phone phone) {
		// TODO Auto-generated method stub
		if (price < phone.price)
			return -1;
		else if (price > phone.price)
			return 1;
		else
			return 0;
		
	}

}