package main;

import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;

public class PhoneServiceArrayListImpl implements PhoneService {

	List<Phone> phoneList = new ArrayList<>();

	// constructor
	public PhoneServiceArrayListImpl() {

	}

	@Override
	public void add(Phone phone) {
		// TODO Auto-generated method stub
		phoneList.add(new Phone(phone.getName(), phone.getYear(), phone.getPrice(), phone.getRam(), phone.getColor(),
				phone.getmanufaturedDate()));

	}

	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		// forEach method
		for (Phone data : phoneList)
			System.out.println(data);

	}

	@Override
	public void search(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sort() {
		// TODO Auto-generated method stub

	}

	@Override
	public void search(LocalDate date) {
		// TODO Auto-generated method stub

	}

	@Override
	public void downloadDetailsAsCSV() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void downloadDetailsAsJSON() throws Exception {
		// TODO Auto-generated method stub

	}

}
