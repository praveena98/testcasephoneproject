package main;

import main.PhoneServiceImpl;
import main.PhoneServiceImpl;
import main.Phone;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

/**
 * A interface specifies the operations on phone object
 * 
 * @author praveena prasannakumar
 *
 *
 */

public class PhoneServiceImpl implements PhoneService {
	private Phone[] phones;
	private int j = 0;
	
	//constructor
	public PhoneServiceImpl() {
		phones=new Phone[100];
		
	}
	
	//constructor accepts array size
	public PhoneServiceImpl(int size) {
		phones=new Phone[size];
		
	}
		

	/**
	 * The method will add a phone
	 * 
	 * @param phone phone to be added
	 *
	 */

	@Override
	public void add(Phone phone) { 
		// TODO Auto-generated method stub
		boolean flag = true;
		if (j == phones.length) {
			System.out.println("size is exceeded");
		} 
		else if(flag==true) {
			
			for (int i = 0; i < j; i++) {
				if (phones[i].equals(phone)) {
					System.out.println("duplicate alert!!!");
					flag = false;				
					//break
				} 				
			}
		}
		if(flag==true)
		{
			phones[j]=phone;
			j++;
		}
	}


	/*
	 * The Method will Search for Phone
	 * 
	 * @param name
	 */

	public void search(String name) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (phones[i].getName().equals(name) || phones[i].getName().equalsIgnoreCase(name)) {
				System.out.println(phones[i]);
				flag = true;
			}
		}
		if (flag == false) {
			System.out.println("no results found");
		}
	}

	/* The Method will Sorting the Phone Details */
	public void sort() {
		Arrays.sort(phones, 0, j);
		displayElements();
	}

	/* The Method will Search the Manufatured date of phone */
	public void search(LocalDate date) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (phones[i].getmanufaturedDate().isAfter(date)) {
				System.out.println(phones[i]);
				flag = true;
			}
		}
		if (flag == false) {
			System.out.println("no results found");
		}
	}

//Disply the phones
	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		if (j == 0) {
			System.out.println("no data");
		} else {
			for (int i = 0; i < j; i++) {
				System.out.println(phones[i]);
			}
		}
	}

	/**
	 * The method downloadDetailsAsCSV download the data as csv format
	 */

	public void downloadDetailsAsCSV() {
		// TODO Auto-generated method stub
		String data;
		try {
			FileWriter file = new FileWriter("downloadPhone.txt");
			for (int i = 0; i < j; i++) {
				data=getCSVString(phones[i]);
				System.out.println(data);
				file.write(data);
				//file.flush();
			}
			file.close();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		}
	}

	/**
	 * The method downloadDetailsAsJSON download the data as json format
	 */

	public void downloadDetailsAsJSON() throws IOException {
		// TODO Auto-generated method stub
			FileWriter file = new FileWriter("downloadPhoneJson.txt");
			for (int i = 0; i < j; i++) {
				file.write(getJSONString(phones[i]));
				file.flush();
			}
			file.close();
	}

}
